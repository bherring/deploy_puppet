module Puppet
  NAME = :puppet

  def self.before(app_deployment, options = Hash.new)
  end

  def self.perform_deploy(app_deployment, options = Hash.new)
    application = app_deployment.application
    environment = options[:resource]
    logger = options[:logger]
    force = options[:force]

    time_stamp = Time.now.strftime('%Y-%m-%d%H_%M_%S')
    temp_dir = Rails.root.join('tmp', "#{time_stamp}-#{application.name.gsub(/\s/, '')}-archive")

    if options[:execute_script]
      begin
        script = app_deployment.application.script.path

        require_relative script
        deploy_parameters = { :temp_dir => temp_dir, :deploy_file => app_deployment.deployment.path, :environment => environment.name, :logger => logger }
        deploy (deploy_parameters)
      rescue Exception => e
        raise InvalidDeployScript.new, e
      end

      logger.debug "Deploying #{app_deployment.deployment_file_name} into #{environment.name} for #{application.name}."
      observers = options[:observer] ? [options[:observer]] : []
      deployment = Deployment.new environment.path, application.repository, app_deployment.version, application.name, logger, Rails.root.join('tmp', application.name), observers
      if !force || (force && deployment.version?(self.version))
        deployment.deploy temp_dir, force, !environment.tag_only
      elsif force
        raise InvalidVersion.new, "A deployment with the version #{app_deployment.version} does not exist."
      end

    else
      if environment.tag_only == false
        deployment.update app_deployment.version
      end
    end
  end

  def self.after(app_deployment, options = Hash.new)
    options[:logger].debug 'Puppet - After'
  end

  def self.error(app_deployment, exception, options = Hash.new)
    options[:logger].debug 'Puppet - Error'

  end

  def self.success(app_deployment, options = Hash.new)
    options[:logger].debug 'Puppet - Success'
    run_puppet options[:resource]
  end

  def self.failure(app_deployment, exception, options = Hash.new)
    options[:logger].debug 'Puppet - Failure'

  end

  private
  def self.run_puppet(environment)
    results = Hash.new
    environment.servers.each do |server|
      #String interpolation
      command = SystemSetting['puppet.command'].gsub('%hostname%', server.puppet_name ? server.puppet_name : server.hostname)

      #Run the Puppet command and return the results
      results[server.hostname] = `#{command}`
    end

    results
  end
end
