class PuppetGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)

  VIEWS_DIRECTORY = Rails.root.join('app', 'views', 'puppet')
  MODULE_DIRECTORY = Rails.root.join('lib', 'modules', 'deployment_methods')
  CSS_DIRECTORY = Rails.root.join('app', 'assets', 'stylesheets')

  def generate_views
    FileUtils.mkdir_p(VIEWS_DIRECTORY) unless File.directory?(VIEWS_DIRECTORY)
    copy_file '_status.html.erb', File.join(VIEWS_DIRECTORY, '_status.html.erb')
    copy_file '_form.html.erb', File.join(VIEWS_DIRECTORY, '_form.html.erb')
    template '_puppet.html.erb', File.join(VIEWS_DIRECTORY, '_puppet.html.erb')
    copy_file "puppet.css.scss", File.join(CSS_DIRECTORY, 'puppet.css.scss')
  end

  def generate_module
    puppet_dir = File.join(MODULE_DIRECTORY, 'puppet')
    FileUtils.mkdir_p(MODULE_DIRECTORY) unless File.directory?(MODULE_DIRECTORY)
    FileUtils.mkdir_p(puppet_dir) unless File.directory?(puppet_dir)

    copy_file File.join('puppet', 'info.yml'), File.join(puppet_dir, 'info.yml')
    copy_file File.join('puppet', 'puppet.rb'), File.join(puppet_dir, 'puppet.rb')
  end
end
